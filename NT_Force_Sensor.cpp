/*
 * @file NT_Force_Sensor.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Force sensor function definitions.
 */

#include "Arduino.h"
#include "NT_Force_Sensor.h"

Force_Sensor_t::Force_Sensor_t()
{
}

void Force_Sensor_t::Configure(int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op)
{
	_Minimum_Sp = Minimum_Sp;
	_Maximum_Sp = Maximum_Sp;
	_Minimum_Op = Minimum_Op;
	_Maximum_Op = Maximum_Op;
}

void Force_Sensor_t::Attach(int Pin)
{
	_Pin = Pin;
}

void Force_Sensor_t::Init()
{
	Enable = TRUE;
}

void Force_Sensor_t::Deinit()
{
	Enable = FALSE;
}

int Force_Sensor_t::Get()
{
	Value_Sp = analogRead(_Pin);

	Value_Op = map(Value_Sp, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op);

	return Value_Op;
}