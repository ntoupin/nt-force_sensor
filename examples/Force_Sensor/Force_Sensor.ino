/*
 * @file Force_sensor.ino
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Simple example of the force sensor utilisation.
 */

#include <NT_Force_Sensor.h>

Force_Sensor_t Force_Sensor_1;

void setup()
{
  Force_Sensor_1.Configure(0, 1024, 0, 1024);
  Force_Sensor_1.Attach(A2);
  Force_Sensor_1.Init();

  Serial.begin(9600);
  while (!Serial);
}

void loop()
{
  Serial.println(Force_Sensor_1.Get());
  delay(1000);
}
