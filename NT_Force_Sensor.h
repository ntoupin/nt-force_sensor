/*
 * @file NT_Force_Sensor.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Force sensor function declarations.
 */

#ifndef NT_Force_Sensor_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Force_Sensor_h

class Force_Sensor_t
{
public:
	Force_Sensor_t();
	bool Enable = FALSE;	
	int Value_Sp = 0;
	int Value_Op = 0;
	void Configure(int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op);
	void Attach(int Pin);
	void Init();
	void Deinit();
	int Get();

private:
	int _Pin;	
	int _Minimum_Sp = 0;
	int _Maximum_Sp = 1024;
	int _Minimum_Op = 0;
	int _Maximum_Op = 1024;
};

#endif